using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using DG.Tweening;

namespace CPU_1 {
    public class Controller_Cpu_1 : MonoBehaviour
    {
        // UI
        public Image imgBg_Screen1;
        public Image btn3, btn2, btn1_5;
        public Sprite sp_btn_3_normal, sp_btn_3_on, sp_btn_2_normal, sp_btn_2_on, sp_btn_1_5_normal, sp_btn_1_5_on;
        public VideoMainController videoMainController;
        private RenderTexture curRenderTex, lastRenderTex;

        private int curPage = 0;

        public Text txtMode;

        private Mode curMode = Mode.Normal;

        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            for(int i = 0; i < Display.displays.Length; i++)
                Display.displays[i].Activate();
            curMode = Mode.Normal;
            videoMainController.Stop();

            SetButtonToNormal();
            txtMode.text = "普通模式";
        }

        public void ResetIdle() {
            curMode = Mode.Normal;
            videoMainController.Stop();

            SetButtonToNormal();
            txtMode.text = "普通模式";
        }

        void SetButtonToNormal() {
            btn3.sprite = sp_btn_3_normal;
            btn2.sprite = sp_btn_2_normal;
            btn1_5.sprite = sp_btn_1_5_normal;
        }

        void PlayButton(int _id) {
            CancelInvoke("SetButtonToNormal");
            videoMainController.StopDelayRelease(_id);

            curPage = 0;
            switch(_id)
            {
                default:
                btn3.sprite = sp_btn_3_on;
                btn2.sprite = sp_btn_2_normal;
                btn1_5.sprite = sp_btn_1_5_normal;
                videoMainController.Play(0);
                Invoke("SetButtonToNormal", 33f);
                //StartCoroutine("DisableAnimAndRelease", 3);
                break;
                case 2:
                btn3.sprite = sp_btn_3_normal;
                btn2.sprite = sp_btn_2_on;
                btn1_5.sprite = sp_btn_1_5_normal;
                videoMainController.Play(1);
                Invoke("SetButtonToNormal", 28f);
                //StartCoroutine("DisableAnimAndRelease", 2);
                break;
                case 1:
                btn3.sprite = sp_btn_3_normal;
                btn2.sprite = sp_btn_2_normal;
                btn1_5.sprite = sp_btn_1_5_on;
                videoMainController.Play(2);
                Invoke("SetButtonToNormal", 27.5f);
                //StartCoroutine("DisableAnimAndRelease", 1);
                break;
            }
            //ShowPage();
        }

        public void On_3_Click() {
            PlayButton(3);
        }

        public void On_2_Click() {
            PlayButton(2);
        }

        public void On_1_5_Click() {
            PlayButton(1);
        }

        public void OnModeClick() {
            if(curMode == Mode.Normal)
            {
                curMode = Mode.Tour;
                txtMode.text = "導賞模式";
                WWWForm form = new WWWForm();
                form.AddField("Start", "1");
                gameUtil.PostController.Post(IP.CPU2, form);
                gameUtil.PostController.Post(IP.CPU3, form);
                gameUtil.PostController.Post(IP.CPU4, form);
                gameUtil.PostController.Post(IP.CPU5, form);
                gameUtil.PostController.Post(IP.CPU6, form);
            } else {
                curMode = Mode.Normal;
                txtMode.text = "普通模式";
                WWWForm form = new WWWForm();
                form.AddField("Start", "0");
                gameUtil.PostController.Post(IP.CPU2, form);
                gameUtil.PostController.Post(IP.CPU3, form);
                gameUtil.PostController.Post(IP.CPU4, form);
                gameUtil.PostController.Post(IP.CPU5, form);
                gameUtil.PostController.Post(IP.CPU6, form);
            }
        }

        public void OnClick() {
            WWWForm form = new WWWForm();
            form.AddField("control", "1");
            //StartCoroutine("Post", form);
            gameUtil.PostController.Post(IP.CPU2, form);
        }
    }
}
