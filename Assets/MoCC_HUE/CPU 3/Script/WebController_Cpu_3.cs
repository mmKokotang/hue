using System.Collections.Generic;
using UnityEngine;
using UniWebServer;

namespace CPU_3
{
    [RequireComponent(typeof(EmbeddedWebServerComponent))]
    public class WebController_Cpu_3 : MonoBehaviour, IWebResource
    {
        public string path = "/control";
        EmbeddedWebServerComponent server;

        public Controller_Cpu_3 controller_Cpu_3;
        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            server = GetComponent<EmbeddedWebServerComponent>();
            server.AddResource(path, this);
        }

        public void HandleRequest (Request request, Response response)
        {
            response.statusCode = 200;
            //response.message = "Someone Login Play";
            Debug.Log(request.body);
            if(request.body == null)
            {
                /*
                string tmpString = html.text.Replace("give_thisPageID", "0");
                tmpString = tmpString.Replace("give_ip", "\'" + "localhost" + "\'");
                tmpString = tmpString.Replace("give_port", "\'" + httpServerComponent.ThisPort.ToString() + "\'");
                response.Write(tmpString);
                */
            } else {
                string[] splitStr = request.body.Split('&');
                for(int i = 0; i < splitStr.Length; i++)
                {
                    Debug.Log(splitStr[i].Split('=')[0] + " = " + splitStr[i].Split('=')[1]);
                    if(splitStr[i].Split('=')[0] == "Reset")
                    {
                        // light white
                        controller_Cpu_3.SetMode(Mode.Normal);
                        controller_Cpu_3.ResetIdle();
                    } else if(splitStr[i].Split('=')[0] == "Start")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            case "0":
                            // light white
                            controller_Cpu_3.SetMode(Mode.Normal);
                            controller_Cpu_3.ResetIdle();

                            // light
                            gameUtil.HueBridge.ChangeLightByID("1", Color.white);
                            gameUtil.HueBridge.ChangeLightByID("2", Color.white);
                            gameUtil.HueBridge.ChangeLightByID("4", Color.white);
                            gameUtil.HueBridge.ChangeLightByID("5", Color.white);
                            // other light
                            ///
                            ///
                            //
                            break;
                            case "1":
                            // light 1 white and close other
                            controller_Cpu_3.SetMode(Mode.Tour);
                            controller_Cpu_3.ResetIdle();

                            // light
                            gameUtil.HueBridge.ChangeLightByID("1", Color.white);
                            gameUtil.HueBridge.ChangeLightByID("2", new Color(0, 0, 0, 0));
                            gameUtil.HueBridge.ChangeLightByID("4", new Color(0, 0, 0, 0));
                            gameUtil.HueBridge.ChangeLightByID("5", new Color(0, 0, 0, 0));
                            break;
                        }
                    } else if(splitStr[i].Split('=')[0] == "Select")
                    {
                        int _i = 0;
                        if(int.TryParse(splitStr[i].Split('=')[1], out _i))
                        {
                            switch(_i) {
                            default:
                            // Select 海平面上升  / 熱浪上升
                                controller_Cpu_3.Select(_i);
                            break;
                            }
                        }
                    } else if(splitStr[i].Split('=')[0] == "NextPage")
                    {
                        int _i = 0;
                        if(int.TryParse(splitStr[i].Split('=')[1], out _i))
                        {
                            switch(_i) {
                            default:
                                controller_Cpu_3.NextPage();
                            break;
                            }
                        }
                    } else if(splitStr[i].Split('=')[0] == "PlayVideo")
                    {
                        int _i = 0;
                        if(int.TryParse(splitStr[i].Split('=')[1], out _i))
                        {
                            switch(_i) {
                                default:
                                    controller_Cpu_3.NextPage();
                                break;
                            }
                        }
                    }
                }
                
            }
        }
    }
}