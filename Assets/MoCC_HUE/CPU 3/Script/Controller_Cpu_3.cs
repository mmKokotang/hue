using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace CPU_3 {
    public class Controller_Cpu_3 : MonoBehaviour
    {
        // UI
        public CanvasGroup imgBg_Up;

        private int curPage;

        // animation
        public Animator anim;

        private Mode curMode = Mode.Normal;

        // Dialogues
        public Text txtSpeak;
        public string[] Dialogues;

        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            for(int i = 0; i < Display.displays.Length; i++)
                Display.displays[i].Activate();

                ResetIdle();
        }


        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                curPage = 7;
                //curPage = 6;
            }
            if(Input.GetKeyUp(KeyCode.A))
            {
                //PlayDialog(0);
                //ShowDialogue(curDialogue);
                //curDialogue++;

                NextPage();
            }
        }

        public void ResetIdle() {
            curPage = 0;
            ShowPage(curPage);
            SetTextDisappear();
            SetCharacterAppear(false);
        }

        public void SetMode(Mode _mode)
        {
            curMode = _mode;
        }

        public void ShowPage(int _page) {
            switch(_page)
            {
                case 0:         // 選 海平面 / 熱浪
                SetCharacterAppear(false);
                imgBg_Up.DOFade(1, 0.5f);
                break;
                case 1:         // 同意 / 不同意
                SetCharacterAppear(true);
                imgBg_Up.DOFade(0, 0.5f);
                PlayDialog(0);
                break;
                case 2:         // 海平面
                PlayDialog(1);
                Invoke("NextPage", 28f);
                break;
                case 3:         // 同意 / 不同意
                PlayDialog(2);
                break;
                case 4:
                PlayDialog(3);
                Invoke("NextPage", 25f);
                break;
                case 5:         // 同意 / 不同意
                PlayDialog(4);
                break;
                case 6:
                PlayDialog(5);
                Invoke("NextPage", 35f);
                break;
                case 7:         // Compare
                PlayDialog(6);
                Invoke("NextPage", 10f);
                Invoke("SetTextDisappear", 10f);
                curPage = -1;
                break;
                case 8:         // 熱浪  同意 / 不同意
                SetCharacterAppear(true);
                imgBg_Up.DOFade(0, 0.5f);
                PlayDialog(7);
                break;
                case 9:         // 熱浪
                PlayDialog(8);
                Invoke("NextPage", 11f);
                break;
                case 10:         // 同意 / 不同意
                PlayDialog(9);
                break;
                case 11:
                PlayDialog(10);
                Invoke("NextPage", 11f);
                break;
                case 12:         // 同意 / 不同意
                PlayDialog(11);
                break;
                case 13:
                PlayDialog(12);
                Invoke("NextPage", 11f);
                break;
                case 14:
                PlayDialog(13);
                Invoke("NextPage", 8f);
                Invoke("SetTextDisappear", 8f);
                curPage = -1;
                break;
                default:
                break;
            }
        }


        // play Dialog
        public void PlayDialog(int _step)
        {
            StopCoroutine("PlayDialogDelay");
            StartCoroutine("PlayDialogDelay", _step);
        }

        private IEnumerator PlayDialogDelay(int _step) {
            float _delayTime = 0.8f;

            switch(_step)
            {
                case 0:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(0);
                break;
                case 1:     // 3度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(1);
                yield return new WaitForSeconds(4 + _delayTime);
                ShowDialogue(2);
                yield return new WaitForSeconds(6 + _delayTime);
                ShowDialogue(3);
                yield return new WaitForSeconds(6 + _delayTime);
                ShowDialogue(4);
                //yield return new WaitForSeconds(8 + _delayTime);
                //SetTextDisappear();
                break;
                case 2:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(5);
                break;
                case 3:     // 2度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(6);
                yield return new WaitForSeconds(5 + _delayTime);
                ShowDialogue(7);
                yield return new WaitForSeconds(8 + _delayTime);
                ShowDialogue(8);
                yield return new WaitForSeconds(7 + _delayTime);
                ShowDialogue(9);
                //yield return new WaitForSeconds(5 + _delayTime);
                //SetTextDisappear();
                break;
                case 4:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(10);
                break;
                case 5:     // 1.5度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(11);
                yield return new WaitForSeconds(5 + _delayTime);
                ShowDialogue(12);
                yield return new WaitForSeconds(8 + _delayTime);
                ShowDialogue(13);
                yield return new WaitForSeconds(6 + _delayTime);
                ShowDialogue(14);
                yield return new WaitForSeconds(8 + _delayTime);
                ShowDialogue(15);
                yield return new WaitForSeconds(3 + _delayTime);
                ShowDialogue(16);
                break;
                case 6:     // Compare
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(17);
                break;
                case 7:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(18);
                break;
                case 8:     // 3度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(19);
                yield return new WaitForSeconds(5 + _delayTime);
                ShowDialogue(20);
                break;
                case 9:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(21);
                break;
                case 10:     // 2度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(22);
                yield return new WaitForSeconds(5 + _delayTime);
                ShowDialogue(23);
                break;
                case 11:     // 同意 / 不同意
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(24);
                break;
                case 12:     // 1.5度
                yield return new WaitForSeconds(_delayTime);
                ShowDialogue(25);
                yield return new WaitForSeconds(6 + _delayTime);
                ShowDialogue(26);
                break;
                case 13:
                yield return new WaitForSeconds(_delayTime);
                SetTextDisappear();
                break;
            }
        }

        public void Select(int _id)
        {
            switch(_id)
            {
                case 0:
                curPage = 0;
                break;
                case 1:
                curPage = 7;
                break;
            }
            NextPage();
        }

        public void NextPage() {
            curPage++;
            ShowPage(curPage);
        }

        public void SetCharacterAppear(bool _b) {
            if(_b)
            {
                anim.transform.position = new Vector3(159f, anim.transform.position.y, anim.transform.position.z);
                anim.transform.DOLocalMoveX(61, 0.5f).SetDelay(0.5f);
            } else {

            }
            anim.gameObject.SetActive(_b);
        }

        public void SetTextDisappear() {
            txtSpeak.text = "";
        }

        public void ShowDialogue(int _id) {
            DOTween.Kill("Speak");
            Dialogues[_id] = Dialogues[_id].Replace("\\n", "\n");
            SetTextDisappear();
            txtSpeak.DOText(Dialogues[_id], Dialogues[_id].Length * 0.05f).SetId("Speak").SetDelay(0.25f).OnStart(()=>{
                anim.SetBool("IsSpeak", true);
            }).OnComplete(()=>{
                anim.SetBool("IsSpeak", false);
            });
        }
    }
}
