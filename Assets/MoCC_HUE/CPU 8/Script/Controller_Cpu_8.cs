using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace CPU_8 {
    public class Controller_Cpu_8 : MonoBehaviour
    {
        // UI
        public Image ImgBg_1, ImgBg_2;
        public Sprite sprite_0_win1, sprite_0_win2;
        public Sprite[] sprite_selected_0_win1;
        public Sprite[] sprite_selected_0_win2;
        
        private int curPage;
        private int selectedID = 0;

        private Mode curMode = Mode.Normal;

        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            for(int i = 0; i < Display.displays.Length; i++)
                Display.displays[i].Activate();
        }

        public void ResetIdle() {
            curPage = 0;
            ShowPage(curPage);
        }

        public void SetMode(Mode _mode)
        {
            curMode = _mode;
        }

        public void ShowPage(int _page) {
            switch(_page)
            {
                case 0:
                ImgBg_1.sprite = sprite_0_win1;
                ImgBg_2.sprite = sprite_0_win2;
                break;
                default:
                int _maxPage = sprite_selected_0_win1.Length;
                if(selectedID == 0)
                {
                    ImgBg_1.sprite = sprite_selected_0_win1[_page-1];
                    ImgBg_2.sprite = sprite_selected_0_win2[_page-1];
                } else {
                    ImgBg_1.sprite = sprite_selected_0_win1[_page-1];
                    ImgBg_2.sprite = sprite_selected_0_win2[_page-1];
                }
                if(_page != 1 && _page < _maxPage)
                    Invoke("NextPage", 3f);
                break;
            }
            
        }

        public void Select(int _id)
        {
            selectedID = _id;
            NextPage();
        }

        public void NextPage() {
            curPage++;
            if(curPage > sprite_selected_0_win1.Length)
            {
                curPage = 0;
                if(curMode == Mode.Normal)
                {
                    ShowPage(curPage);
                } else {
                    // light on next area
                }
            } else {
                ShowPage(curPage);
                //
            }
        }
    }
}
