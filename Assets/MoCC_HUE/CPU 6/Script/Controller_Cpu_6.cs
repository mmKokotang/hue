using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Video;

namespace CPU_6 {
    public class Controller_Cpu_6 : MonoBehaviour
    {
        // UI
        public RawImage ImgBg_1;
        public RawImage ImgBg_2;
        public Texture[] SatelliteImage;
        private int curSatImg = 0;

        public AnalyzingController analyzingController;
        
        public Image btn;
        public CanvasGroup btnGrp;
        public Sprite btn_select, btn_agree;

        private int curPage;

        private Mode curMode = Mode.Normal;
        
        private bool isCanClick = true;

        public VideoMainController videoMainController;

        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            for(int i = 0; i < Display.displays.Length; i++)
                Display.displays[i].Activate();
            
            ImgBg_1.texture = SatelliteImage[curSatImg];
            StartCoroutine("SatelliteImageLoop");
            analyzingController.StartAnalyzingAnim();
        }

        public void ResetIdle() {
            curPage = 0;
            isCanClick = true;
            ShowPage(curPage);
            videoMainController.Stop();
        }

        public void SetMode(Mode _mode)
        {
            curMode = _mode;
        }

        private IEnumerator SatelliteImageLoop() {
            yield return new WaitForSeconds(5f);
            curSatImg++;
            if(curSatImg >= SatelliteImage.Length)
            {
                curSatImg = 0;
            }
            ImgBg_1.texture = SatelliteImage[curSatImg];
            StartCoroutine("SatelliteImageLoop");
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                curPage = 7;
                //curPage = 6;
            }
            if(Input.GetKeyDown(KeyCode.A))
            {
                NextPage();
            }
        }
        
        public void ShowPage(int _page) {
            switch(_page)
            {
                case 0:         // 選 海平面 / 熱浪
                btn.sprite = btn_select;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                isCanClick = true;
                break;
                case 1:         // 同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                isCanClick = true;
                break;
                case 2:         // 海平面
                btn.gameObject.SetActive(false);
                PlayVideo(0);
                Invoke("NextPage", 28f);
                break;
                case 3:         // 同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                StopVideo();
                break;
                case 4:
                btn.gameObject.SetActive(false);
                PlayVideo(1);
                Invoke("NextPage", 25f);
                break;
                case 5:         // 同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                StopVideo();
                break;
                case 6:
                btn.gameObject.SetActive(false);
                PlayVideo(2);
                Invoke("NextPage", 35f);
                break;
                case 7:         // Compare
                btn.gameObject.SetActive(false);
                PlayVideo(3);
                Invoke("NextPage", 10f);
                curPage = -1;
                break;
                case 8:         // 熱浪  同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                StopVideo();
                break;
                case 9:         // 熱浪
                btn.gameObject.SetActive(false);
                PlayVideo(4);
                Invoke("NextPage", 11f);
                break;
                case 10:         // 同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                StopVideo();
                break;
                case 11:         
                btn.gameObject.SetActive(false);
                PlayVideo(5);
                Invoke("NextPage", 11f);
                break;
                case 12:         // 同意 / 不同意
                btn.sprite = btn_agree;
                btn.SetNativeSize();
                btn.gameObject.SetActive(true);
                StopVideo();
                break;
                case 13:         
                btn.gameObject.SetActive(false);
                PlayVideo(6);
                Invoke("NextPage", 11f);
                break;
                case 14:         
                btn.gameObject.SetActive(false);
                PlayVideo(7);
                Invoke("NextPage", 8f);
                curPage = -1;
                break;
                default:
                break;
            }
        }

        public void Select(int _id)
        {
            switch(_id)
            {
                case 0:
                curPage = 0;
                break;
                case 1:
                curPage = 7;
                break;
            }
            NextPage();
        }

        public void NextPage() {
            curPage++;
            if(curPage == 15)
            {
                curPage = 0;
            }
            ShowPage(curPage);
            if( curPage == 0 || 
                curPage == 3 || 
                curPage == 5 || 
                curPage == 8 || 
                curPage == 10 || 
                curPage == 12)
                {
                    analyzingController.StartAnalyzingAnim();
                    isCanClick = true;
                }
            /*
            if(curPage > sprite_selected_0_win2.Length)
            {
                curPage = 0;
                if(curMode == Mode.Normal)
                {
                    ShowPage(curPage);
                } else {
                    // light on next area
                }
            } else {
                ShowPage(curPage);
                //
            }
            */
        }

        public void PlayVideo(int _id) {
            videoMainController.StopDelayRelease(_id);
            videoMainController.Play(_id);
        }
        public void StopVideo() {
            videoMainController.Stop();
        }
        
        public void OnBtnClick() {
            Debug.Log("OnBtnClick");
            if(!isCanClick)
                return;
            isCanClick = false;
            WWWForm form;
            switch(curPage)
            {
                case 0:
                /*
                selectedID = 1;
                NextPage();

                form = new WWWForm();
                form.AddField("Select", "1");
                gameUtil.PostController.Post(IP.CPU2, form);
                gameUtil.PostController.Post(IP.CPU4, form);
                gameUtil.PostController.Post(IP.CPU5, form);
                */

                StartCoroutine(GameUtil.GetInstance().PostController.CheckAllConnected((_success, _str)=>{
                    if(_success)
                    {
                        //====== self function =======//
                        //NextPage();
                        
                        //============================//

                        //====== network function =====//

                        form = new WWWForm();
                        form.AddField("Select", "1");
                        //form.AddField("PlayVideo", "0");
                        //gameUtil.PostController.Post(IP.CPU2, form);
                        //gameUtil.PostController.Post(IP.CPU3, form);
                        //gameUtil.PostController.Post(IP.CPU4, form);
                        //gameUtil.PostController.Post(IP.CPU5, form);


                        //gameUtil.PostController.Post(IP.CPU6, form);


                        //gameUtil.PostController.Post(IP.CPU7, form);
                        //gameUtil.PostController.Post(IP.CPU8, form);
                        //gameUtil.PostController.Post(IP.CPU9, form);
                        //gameUtil.PostController.Post(IP.CPU10, form);
                        //gameUtil.PostController.Post(IP.CPU11, form);

                        //gameUtil.HueBridge.ChangeLightByID("1", Color.red);
                        NextPage();
                        isCanClick = true;

                        //============================//
                    } else {
                        Debug.LogError(_str);
                        isCanClick = true;
                    }
                }));
                break;
                default:
                /*
                NextPage();
                
                form = new WWWForm();
                form.AddField("Next", "0");
                gameUtil.PostController.Post(IP.CPU2, form);
                gameUtil.PostController.Post(IP.CPU4, form);
                gameUtil.PostController.Post(IP.CPU5, form);
                
                // change to red light
                gameUtil.HueBridge.ChangeLightByID("1", Color.red);
                */

                StartCoroutine(GameUtil.GetInstance().PostController.CheckAllConnected((_success, _str)=>{
                    if(_success)
                    {
                        //====== self function =======//
                        NextPage();
                        //============================//

                        //====== network function =====//
                        /*
                        form = new WWWForm();
                        form.AddField("Next", "0");
                        gameUtil.PostController.Post(IP.CPU2, form);
                        gameUtil.PostController.Post(IP.CPU3, form);
                        gameUtil.PostController.Post(IP.CPU4, form);
                        gameUtil.PostController.Post(IP.CPU5, form);
                        gameUtil.PostController.Post(IP.CPU6, form);
                        gameUtil.PostController.Post(IP.CPU7, form);
                        gameUtil.PostController.Post(IP.CPU8, form);
                        gameUtil.PostController.Post(IP.CPU9, form);
                        gameUtil.PostController.Post(IP.CPU10, form);
                        gameUtil.PostController.Post(IP.CPU11, form);
                        */

                        //gameUtil.HueBridge.ChangeLightByID("1", Color.red);
                        //============================//
                    } else {
                        Debug.LogError(_str);
                        isCanClick = true;
                    }
                }));
                break;
            }
        }
    }
}
