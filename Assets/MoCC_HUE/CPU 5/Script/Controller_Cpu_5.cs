using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Video;

namespace CPU_5 {
    public class Controller_Cpu_5 : MonoBehaviour
    {
        // UI
        public RawImage ImgBg_1;
        public RawImage ImgBg_2;
        public Texture[] SatelliteImage;
        private int curSatImg = 0;

        public AnalyzingController analyzingController;

        public Text txtDescript;
        public string[] Dialogues;

        private int curPage;

        private Mode curMode = Mode.Normal;

        public VideoMainController videoMainController;

        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            for(int i = 0; i < Display.displays.Length; i++)
                Display.displays[i].Activate();
            ImgBg_1.texture = SatelliteImage[curSatImg];
            StartCoroutine("SatelliteImageLoop");
            analyzingController.StartAnalyzingAnim();
        }

        public void ResetIdle() {
            curPage = 0;
            ShowPage(curPage);
            videoMainController.Stop();
        }

        public void SetMode(Mode _mode)
        {
            curMode = _mode;
        }

        private IEnumerator SatelliteImageLoop() {
            yield return new WaitForSeconds(5f);
            curSatImg++;
            if(curSatImg >= SatelliteImage.Length)
            {
                curSatImg = 0;
            }
            ImgBg_1.texture = SatelliteImage[curSatImg];
            StartCoroutine("SatelliteImageLoop");
        }

        /// <summary>
        /// Update is called every frame, if the MonoBehaviour is enabled.
        /// </summary>
        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Alpha1))
            {
                curPage = 7;
                //curPage = 6;
            }
            if(Input.GetKeyDown(KeyCode.A))
            {
                NextPage();
            }
            if(Input.GetKeyDown(KeyCode.G))
            {
                PlayVideo(1);
            }
            if(Input.GetKeyDown(KeyCode.H))
            {
                PlayVideo(2);
            }
            if(Input.GetKeyDown(KeyCode.J))
            {
                StopVideo();
            }
        }

        public void ShowPage(int _page) {
            
            switch(_page)
            {
                case 0:         // 選 海平面 / 熱浪
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                break;
                case 1:         // 同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                break;
                case 2:         // 海平面
                txtDescript.gameObject.SetActive(false);
                PlayVideo(0);
                Invoke("NextPage", 28f);
                break;
                case 3:         // 同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                StopVideo();
                break;
                case 4:
                txtDescript.gameObject.SetActive(false);
                PlayVideo(1);
                Invoke("NextPage", 25f);
                break;
                case 5:         // 同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                StopVideo();
                break;
                case 6:
                txtDescript.gameObject.SetActive(false);
                PlayVideo(2);
                Invoke("NextPage", 35f);
                break;
                case 7:         // Compare
                txtDescript.gameObject.SetActive(false);
                PlayVideo(3);
                Invoke("NextPage", 10f);
                curPage = -1;
                break;
                case 8:         // 熱浪  同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                StopVideo();
                break;
                case 9:         // 熱浪
                txtDescript.gameObject.SetActive(false);
                PlayVideo(4);
                Invoke("NextPage", 11f);
                break;
                case 10:         // 同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                StopVideo();
                break;
                case 11:         
                txtDescript.gameObject.SetActive(false);
                PlayVideo(5);
                Invoke("NextPage", 11f);
                break;
                case 12:         // 同意 / 不同意
                txtDescript.gameObject.SetActive(true);
                txtDescript.text = Dialogues[_page].Replace("\\n", "\n");
                StopVideo();
                break;
                case 13:         
                txtDescript.gameObject.SetActive(false);
                PlayVideo(6);
                Invoke("NextPage", 11f);
                break;
                case 14:         
                txtDescript.gameObject.SetActive(false);
                PlayVideo(7);
                Invoke("NextPage", 8f);
                curPage = -1;
                break;
                default:
                break;
            }
        }

        public void Select(int _id)
        {
            switch(_id)
            {
                case 0:
                curPage = 0;
                break;
                case 1:
                curPage = 7;
                break;
            }
            NextPage();
        }

        public void NextPage() {
            curPage++;
            ShowPage(curPage);
            if( curPage == 0 || 
                curPage == 3 || 
                curPage == 5 || 
                curPage == 8 || 
                curPage == 10 || 
                curPage == 12)
                analyzingController.StartAnalyzingAnim();
            /*
            if(curPage > sprite_selected_0_win2.Length)
            {
                curPage = 0;
                if(curMode == Mode.Normal)
                {
                    ShowPage(curPage);
                } else {
                    // light on next area
                }
            } else {
                ShowPage(curPage);
                //
            }
            */
        }

        public void PlayVideo(int _id) {
            txtDescript.gameObject.SetActive(false);
            videoMainController.StopDelayRelease(_id);
            videoMainController.Play(_id);
        }

        public void StopVideo() {
            videoMainController.Stop();
        }

        IEnumerator UpdateLight() {
            gameUtil.HueBridge.ChangeAreaLight(1, Color.red);
            yield return new WaitForSeconds(28);
            /*
            gameUtil.HueBridge.ChangeLightByID("1", new Color(1f, 150f/255f, 0f, 1f));
            yield return new WaitForSeconds(5);
            gameUtil.HueBridge.ChangeLightByID("1", Color.yellow);
            if(curMode == Mode.Normal)
            {
                yield return new WaitForSeconds(5);
                gameUtil.HueBridge.ChangeLightByID("1", Color.white);
            } else {
                yield return new WaitForSeconds(5);
                gameUtil.HueBridge.ChangeLightByID("1", new Color(1, 1, 1, 0));
            }
            */
        }
    }
}
