using System.Collections.Generic;
using UnityEngine;
using UniWebServer;

namespace CPU_5
{
    [RequireComponent(typeof(EmbeddedWebServerComponent))]
    public class WebController_Cpu_5 : MonoBehaviour, IWebResource
    {
        public string path = "/control";
        EmbeddedWebServerComponent server;

        public Controller_Cpu_5 controller_Cpu_5;
        private GameUtil gameUtil;
        // Start is called before the first frame update
        void Start()
        {
            gameUtil = GameUtil.GetInstance();
            server = GetComponent<EmbeddedWebServerComponent>();
            server.AddResource(path, this);
        }

        public void HandleRequest (Request request, Response response)
        {
            response.statusCode = 200;
            //response.message = "Someone Login Play";
            Debug.Log(request.body);
            if(request.body == null)
            {
                /*
                string tmpString = html.text.Replace("give_thisPageID", "0");
                tmpString = tmpString.Replace("give_ip", "\'" + "localhost" + "\'");
                tmpString = tmpString.Replace("give_port", "\'" + httpServerComponent.ThisPort.ToString() + "\'");
                response.Write(tmpString);
                */
            } else {
                string[] splitStr = request.body.Split('&');
                for(int i = 0; i < splitStr.Length; i++)
                {
                    Debug.Log(splitStr[i].Split('=')[0] + " = " + splitStr[i].Split('=')[1]);
                    if(splitStr[i].Split('=')[0] == "Reset")
                    {
                        // light white
                        controller_Cpu_5.SetMode(Mode.Normal);
                        controller_Cpu_5.ResetIdle();
                    } else if(splitStr[i].Split('=')[0] == "Start")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            case "0":
                            // light white
                            controller_Cpu_5.SetMode(Mode.Normal);
                            controller_Cpu_5.ResetIdle();

                            // light
                            //gameUtil.HueBridge.ChangeLightByID("1", new Color(0, 0, 0, 0));
                            gameUtil.HueBridge.ChangeAreaLight(1, new Color(1, 1, 1, 0.5f));
                            gameUtil.HueBridge.ChangeAreaLight(2, new Color(1, 1, 1, 0f));
                            gameUtil.HueBridge.ChangeAreaLight(3, new Color(1, 1, 1, 0f));
                            // other light
                            ///
                            ///
                            //
                            break;
                            case "1":
                            // light 1 white and close other
                            controller_Cpu_5.SetMode(Mode.Tour);
                            controller_Cpu_5.ResetIdle();

                            // light
                            gameUtil.HueBridge.ChangeAreaLight(1, new Color(1, 1, 1, 1f));
                            gameUtil.HueBridge.ChangeAreaLight(2, new Color(1, 1, 1, 0f));
                            gameUtil.HueBridge.ChangeAreaLight(3, new Color(1, 1, 1, 0f));
                            break;
                        }
                    } else if(splitStr[i].Split('=')[0] == "Select")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            case "0":
                            // Select 海平面上升
                            controller_Cpu_5.Select(0);
                            break;
                            case "1":
                            // Select 嚴重熱浪
                            controller_Cpu_5.Select(1);
                            break;
                        }
                    } else if(splitStr[i].Split('=')[0] == "Next")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            default:
                            controller_Cpu_5.NextPage();
                            break;
                        }
                    } else if(splitStr[i].Split('=')[0] == "PlayVideo")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            default:
                            controller_Cpu_5.PlayVideo(0);
                            break;
                        }
                    }
                }
                
            }
        }
    }
}