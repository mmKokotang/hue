using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FollowVideo : MonoBehaviour
{
    private RawImage rawImage;
    public RawImage Target;
    // Start is called before the first frame update
    void Start()
    {
        rawImage = GetComponent<RawImage>();
        rawImage.texture = Target.texture;
    }

    // Update is called once per frame
    void Update()
    {
        if(rawImage.texture != Target.texture)
            rawImage.texture = Target.texture;
        if(rawImage.texture == null)
            rawImage.enabled = false;
        else
            rawImage.enabled = true;
    }
}
