using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
[RequireComponent(typeof(VideoPlayer))]
public class VideoController : MonoBehaviour
{
    [HideInInspector]
    public VideoPlayer videoPlayer;
    public int PlayAtFrame = 0;
    public float FrameStopBeforeTime = 0;

    [HideInInspector]
    public RawImage rawImage;
    // Start is called before the first frame update
    void Start()
    {
        videoPlayer = GetComponent<VideoPlayer>();
    }
    void OnApplicationQuit()
    {
        videoPlayer.targetTexture.Release();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Play() {
        StopCoroutine("AutoStopAfterPlay");
        StopCoroutine("Release");
        videoPlayer.Play();
        videoPlayer.frame = PlayAtFrame;
        StartCoroutine("AutoStopAfterPlay");
    }

    public void Stop() {
        StopCoroutine("AutoStopAfterPlay");
        StopCoroutine("Release");
        if(videoPlayer  == null)
            return;
        videoPlayer.Stop();
        videoPlayer.targetTexture.Release();
        
        if(rawImage != null && rawImage.texture == videoPlayer.targetTexture)
        {
            rawImage.gameObject.SetActive(false);
            rawImage = null;
        } else if(rawImage != null)
        {
            rawImage = null;
        }
    }

    public void CancelAutoStop() {
        StopCoroutine("AutoStopAfterPlay");
    }

    public void StopDelayRelease() {
        StopCoroutine("AutoStopAfterPlay");
        StartCoroutine("Release");
    }

    IEnumerator Release() {
        videoPlayer.Stop();
        yield return new WaitForSeconds(0.5f);
        videoPlayer.targetTexture.Release();
        if(rawImage != null && rawImage.texture == videoPlayer.targetTexture)
        {
            rawImage.gameObject.SetActive(false);
            rawImage = null;
        } else if(rawImage != null)
        {
            rawImage = null;
        }
    }

    IEnumerator AutoStopAfterPlay() {
        yield return new WaitForSeconds((float)videoPlayer.length - FrameStopBeforeTime);
        Stop();
    }
}
