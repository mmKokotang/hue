using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PostController : MonoBehaviour
{
    private int TimeOutTime = 2;
    private GameUtil gameUtil;
    // Start is called before the first frame update
    void Start()
    {
        gameUtil = GameUtil.GetInstance();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Escape))
        {
            WWWForm form;
            
            StartCoroutine(GameUtil.GetInstance().PostController.CheckAllConnected((_success, _str)=>{
                    if(_success)
                    {

                        form = new WWWForm();
                        form.AddField("Reset", "");
                        gameUtil.PostController.Post(IP.CPU1, form);
                        gameUtil.PostController.Post(IP.CPU2, form);
                        gameUtil.PostController.Post(IP.CPU3, form);
                        gameUtil.PostController.Post(IP.CPU4, form);
                        gameUtil.PostController.Post(IP.CPU5, form);
                        gameUtil.PostController.Post(IP.CPU6, form);
                        gameUtil.PostController.Post(IP.CPU7, form);
                        gameUtil.PostController.Post(IP.CPU8, form);
                        gameUtil.PostController.Post(IP.CPU9, form);
                        gameUtil.PostController.Post(IP.CPU10, form);
                        //============================//
                    } else {
                        Debug.LogError(_str);
                    }
                }));
        }
    }

    public IEnumerator CheckAllConnected(Action<bool, string> _callBack) {
        WWWForm _form = new WWWForm();
        _form.AddField("", "");
        yield return new WaitForSeconds(0);
        /*
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU1 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            
            yield return www.SendWebRequest();
            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU1);
                _callBack(false, "CPU1(" + IP.CPU1 + ") " + www.error);
                yield break;
            }
        } */
        
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU2 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU2);
                _callBack(false, "CPU2(" + IP.CPU2 + ") " + www.error);
                yield break;
            }
        }
        /*
        
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU3 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU3);
                _callBack(false, "CPU3(" + IP.CPU3 + ") " + www.error);
                yield break;
            }
        }
        */
        
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU4 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU4);
                _callBack(false, "CPU4(" + IP.CPU4 + ") " + www.error);
                yield break;
            }
        }
        
        /*
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU5 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU5);
                _callBack(false, "CPU5(" + IP.CPU5 + ") " + www.error);
                yield break;
            }
        }
        */
        
        /*
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU6 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU6);
                _callBack(false, "CPU6(" + IP.CPU6 + ") " + www.error);
                yield break;
            }
        }
        */
        
        /*
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU7 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU7);
                _callBack(false, "CPU7(" + IP.CPU7 + ") " + www.error);
                yield break;
            }
        }
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU8 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU8);
                _callBack(false, "CPU8(" + IP.CPU8 + ") " + www.error);
                yield break;
            }
        }
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU9 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU9);
                _callBack(false, "CPU9(" + IP.CPU9 + ") " + www.error);
                yield break;
            }
        }
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + IP.CPU10 + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
                //_callBack(true, "");
            }
            else
            {
                Debug.Log(www.error + " " + IP.CPU10);
                _callBack(false, "CPU10(" + IP.CPU10 + ") " + www.error);
                yield break;
            }
        }
        */
        /*
        byte[] bytes = System.Text.Encoding.ASCII.GetBytes ("0");
		using (UnityWebRequest www = UnityWebRequest.Put("http://"+ IP.LightIP +  "/api/" + gameUtil.HueBridge.Username + "/lights/" + "1" + "/state", bytes))
        {
            www.timeout = TimeOutTime;
            Debug.Log(www.timeout);
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.Success)
            {
            }
            else
            {
                Debug.Log(www.error);
                _callBack(false, "Light(" + "1" + ") " + www.error);
                yield break;
            }
        }
        */
        

        _callBack(true, "");
        
        
    }

    public void Post(string _ip, WWWForm _form) {
        StartCoroutine(PostToNetWork(_ip, _form));
    }

    public IEnumerator PostToNetWork(string _ip, WWWForm _form, Action<bool, string> _callBack = null) {
        using (UnityWebRequest www = UnityWebRequest.Post("http://" + _ip + ":8079/control", _form))
        {
            www.timeout = TimeOutTime;
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error + ":" + _ip);
                if(_callBack != null)
                    _callBack(false, www.error);
            }
            else
            {
                //Debug.Log("Form upload complete!");
                if(_callBack != null)
                    _callBack(true, "");
            }
        }
    }
}
