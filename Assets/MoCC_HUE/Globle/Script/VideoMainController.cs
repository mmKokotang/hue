using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoMainController : MonoBehaviour
{
    public RawImage imgVideo_down, imgVideo_up;
    private RenderTexture curRenderTex, lastRenderTex;
    
    private VideoController[] videoControllers;
    // Start is called before the first frame update
    void Start()
    {
        videoControllers = new VideoController[transform.childCount];
        for(int i = 0; i < transform.childCount; i++)
        {
            videoControllers[i] = transform.GetChild(i).GetComponent<VideoController>();
        }
        if(imgVideo_down.texture != null) {
            RenderTexture r = (RenderTexture)imgVideo_down.texture;
            r.Release();
        }
        if(imgVideo_up.texture != null) {
            RenderTexture r = (RenderTexture)imgVideo_up.texture;
            r.Release();
        }
    }

    public float Play(int _id) {
        lastRenderTex = curRenderTex;
        imgVideo_down.texture = lastRenderTex;
        if(imgVideo_down.texture == null)
            imgVideo_down.color = new Color(1, 1, 1, 0);
        else
            imgVideo_down.color = new Color(1, 1, 1, 1);

        curRenderTex = videoControllers[_id].videoPlayer.targetTexture;
        //imgVideo_3.gameObject.SetActive(true);
        videoControllers[_id].Play();
        videoControllers[_id].rawImage = imgVideo_up;
        imgVideo_up.texture = curRenderTex;
        if(imgVideo_down.texture != null)
            imgVideo_down.gameObject.SetActive(true);
        imgVideo_up.gameObject.SetActive(true);
        CancelInvoke("ImgVideoDownDisable");
        Invoke("ImgVideoDownDisable", 1f);

        float _duration = (float)videoControllers[_id].videoPlayer.clip.length;
        return _duration;
    }

    public void Stop() {
        for(int i = 0; i < videoControllers.Length; i++)
        {
            videoControllers[i].Stop();
        }
    }

    public void StopDelayRelease(int _notReleaseID) {
        for(int i = 0; i < videoControllers.Length; i++)
        {
            if(i != _notReleaseID)
                videoControllers[i].StopDelayRelease();
            else
                videoControllers[i].CancelAutoStop();
        }
    }

    public void ImgVideoDownDisable() {
        imgVideo_down.gameObject.SetActive(false);
    }
}
