using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class AnalyzingController : MonoBehaviour
{
    
    public CanvasGroup analyzingGrp;
    public Text txtAnalyzing;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    private IEnumerator AnalyzingAnim() {
        txtAnalyzing.text = "分析中";
        yield return new WaitForSeconds(0.1f);
        txtAnalyzing.text = "分析中.";
        yield return new WaitForSeconds(0.1f);
        txtAnalyzing.text = "分析中..";
        yield return new WaitForSeconds(0.1f);
        txtAnalyzing.text = "分析中...";
        yield return new WaitForSeconds(0.3f);
        StartCoroutine("AnalyzingAnim");
    }

    public void StartAnalyzingAnim() {
        analyzingGrp.DOFade(1, 0);
        StartCoroutine("AnalyzingAnim");
        Invoke("StopAnalyzingAnim", 1f);
    }

    public void StopAnalyzingAnim() {
        analyzingGrp.DOFade(0, 0);
        StopCoroutine("AnalyzingAnim");
    }
}
