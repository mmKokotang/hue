using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideBtnController : MonoBehaviour
{
    public Camera camera2;

    private bool isLeftTopClick = false;
    private int touchCount = 0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(camera2.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10)), Vector2.zero);
        if(hit.collider != null)
        {
            if(hit.collider.name == "Btn1Hiding")
            {
                isLeftTopClick = true;
            }
        }

        if (Input.touchCount > 0)
        {
            Touch touch0 = Input.GetTouch(0);

            // Handle finger movements based on touch phase.
            switch (touch0.phase)
            {
                // Record initial touch position.
                case TouchPhase.Began:
                    RaycastHit2D hit0 = Physics2D.Raycast(camera2.ScreenToWorldPoint(new Vector3(touch0.position.x, touch0.position.y, 10)), Vector2.zero);
                    if(hit0.collider != null)
                    {
                        if(hit0.collider.name == "Btn1Hiding")
                        {
                            isLeftTopClick = true;
                        }
                    }
                    
                    break;

                // Report that a direction has been chosen when the finger is lifted.
                case TouchPhase.Ended:
                    isLeftTopClick = false;
                    touchCount = 0;
                    break;
            }

            if(isLeftTopClick)
            {
                Touch touch1 = Input.GetTouch(1);

                // Handle finger movements based on touch phase.
                switch (touch1.phase)
                {
                    // Record initial touch position.
                    case TouchPhase.Began:
                        RaycastHit2D hit1 = Physics2D.Raycast(camera2.ScreenToWorldPoint(new Vector3(touch1.position.x, touch1.position.y, 10)), Vector2.zero);
                        if(hit1.collider != null)
                        {
                            if(hit1.collider.name == "Btn2Hiding")
                            {
                                touchCount++;
                            }
                        }

                        if(touchCount == 3)
                        {
                            GetComponent<CPU_1.Controller_Cpu_1>().OnModeClick();
                        }
                        
                        break;

                    // Report that a direction has been chosen when the finger is lifted.
                    case TouchPhase.Ended:
                        isLeftTopClick = false;
                        break;
                }
            }
        }
    }
}
