﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;

using MiniJSON;

//[ExecuteInEditMode]
public class HueLamp : MonoBehaviour {
	public string devicePath;
	public bool on = true;
	public Color color = Color.white;

	private bool oldOn;
	private Color oldColor;

	private GameUtil gameUtil;

	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		gameUtil = GameUtil.GetInstance();
	}

	void Update () {
		if (oldOn != on || oldColor != color) {
			gameUtil.HueBridge.ChangeLightByID(devicePath, color);
		}
		oldOn = on;
		oldColor = color;
	}
}
