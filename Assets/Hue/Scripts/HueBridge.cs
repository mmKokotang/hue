﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine.Networking;

using MiniJSON;

using DG.Tweening;

public class HueBridge : MonoBehaviour {
	public int portNumber = 8000;
	private string username = "J2aG6utcjZhkD05JUu7W1fqzpkvZio4YxsvpTtN0";
	public string Username {
		get { return username; }
	}

	static HueBridge bridge;

	private float alpha = 0.5f;
	private Color curColor;
	private int AreaID = 0;

	// Use this for initialization
	void Start () {
		bridge = this;

		DOTween.To(x => alpha = x, 0.5f, 1f, 1f).SetLoops(-1, LoopType.Yoyo);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.U))
		{
			Debug.Log("U " + IP.LightIP);
			ChangeLightByID("1", Color.red);
		}
		if(Input.GetKeyUp(KeyCode.O))
		{
			Debug.Log("O" + IP.LightIP);
			ChangeLightByID("1", Color.green);
		}
	}

	public void DiscoverLights() {
        HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://" + IP.LightIP + "/api/" + username + "/lights");
		HttpWebResponse response = (HttpWebResponse)request.GetResponse ();
        Debug.Log("http" + IP.LightIP + portNumber + "/api/" + username + "/lights");

		System.IO.Stream stream = response.GetResponseStream();
		System.IO.StreamReader streamReader = new System.IO.StreamReader(stream, System.Text.Encoding.UTF8);
		
		var lights = (Dictionary<string, object>)Json.Deserialize (streamReader.ReadToEnd());
		foreach (string key in lights.Keys) {
			var light = (Dictionary<string, object>)lights[key];

			foreach (HueLamp hueLamp in GetComponentsInChildren<HueLamp>()) {
				if (hueLamp.devicePath.Equals(key)) goto Found;
			}
			
			if (light["type"].Equals("Extended color light")) {
				GameObject gameObject = new GameObject();
				gameObject.name = (string)light["name"];
				gameObject.transform.parent = transform;
				gameObject.AddComponent<HueLamp>();
				HueLamp lamp = gameObject.GetComponent<HueLamp>();
				lamp.devicePath = key;
			}

		Found:
			;
		}
	}

	public void ChangeLightByID(string _id, Color _color) {
		StartCoroutine(ChangeLightByID(_id, _color, null));
		/*
		byte[] bs= System.Text.Encoding.UTF8.GetBytes("string");
		HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://"+ bridge.hostName +  "/api/" + bridge.username + "/lights/" + _id + "/state");
		//Debug.Log("http" + bridge.hostName + bridge.portNumber + "/api/" + bridge.username + "/lights/" + _id + "/state");
		request.Method = "PUT";

		Vector3 hsv = HSVFromRGB (_color);

		var state = new Dictionary<string, object> ();
		state["on"] = true;
		state ["hue"] = (int)(hsv.x / 360.0f * 65535.0f);
		state ["sat"] = (int)(hsv.y * 255.0f);
		state ["bri"] = (int)(hsv.z * 255.0f);
		if ((int)(hsv.z * 255.0f) == 0) state["on"] = false;

		byte[] bytes = System.Text.Encoding.ASCII.GetBytes (Json.Serialize (state));
		request.ContentLength = bytes.Length;
		
		System.IO.Stream s = request.GetRequestStream ();
		s.Write (bytes, 0, bytes.Length);
		s.Close ();

		HttpWebResponse response = (HttpWebResponse)request.GetResponse ();

		response.Close ();
		*/
	}

	public IEnumerator ChangeLightByID(string _id, Color _color, Action<bool, string> _callback = null)
	{
		Vector3 hsv = HSVFromRGB (_color);

		var state = new Dictionary<string, object> ();
		state["on"] = true;
		state ["hue"] = (int)(hsv.x / 360.0f * 65535.0f);
		state ["sat"] = (int)(hsv.y * 255.0f);
		state ["bri"] = (int)(hsv.z * 255.0f);
		if ((int)(hsv.z * 255.0f) == 0) state["on"] = false;

		byte[] bytes = System.Text.Encoding.ASCII.GetBytes (Json.Serialize (state));
		using (UnityWebRequest www = UnityWebRequest.Put("http://"+ IP.LightIP +  "/api/" + bridge.username + "/lights/" + _id + "/state", bytes))
        {
			www.timeout = 2;
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
				if(_callback != null)
                	_callback(false, www.error);
            }
            else
            {
                //Debug.Log("Form upload complete! " + "http://"+ IP.LightIP +  "/api/" + bridge.username + "/lights/" + _id + "/state");
				if(_callback != null)
                	_callback(true, "");
            }
        }
	}


	public static Vector3 HSVFromRGB(Color rgb) {
		float max = Mathf.Max(rgb.r, Mathf.Max(rgb.g, rgb.b));
		float min = Mathf.Min(rgb.r, Mathf.Min(rgb.g, rgb.b));
		
		float brightness = rgb.a;
		
		float hue, saturation;
		if (max == min) {
			hue = 0f;
			saturation = 0f;
		} else {
			float c = max - min;
			if (max == rgb.r) {
				hue = (rgb.g - rgb.b) / c;
			} else if (max == rgb.g) {
				hue = (rgb.b - rgb.r) / c + 2f;
			} else {
				hue = (rgb.r - rgb.g) / c + 4f;
			}

			hue *= 60f;
			if (hue < 0f) {
				hue += 360f;
			}
			
			saturation = c / max;
		}

		return new Vector3(hue, saturation, brightness);
	}

	public void ChangeAreaLight(int _areaID, Color _color)
	{
		CancelInvoke("UpdateColor");
		curColor = _color;
		AreaID = _areaID;
		UpdateColor();
		/*
		switch(_areaID) 
		{
			case 1:
			curColor = _color;
			UpdateColor();
			ChangeLightByID("1", _color);
			ChangeLightByID("2", _color);
			ChangeLightByID("4", _color);
			ChangeLightByID("5", _color);
			break;
			case 2:
			ChangeLightByID("1", _color);
			ChangeLightByID("2", _color);
			ChangeLightByID("4", _color);
			ChangeLightByID("5", _color);
			break;
			case 3:
			ChangeLightByID("1", _color);
			ChangeLightByID("2", _color);
			ChangeLightByID("4", _color);
			ChangeLightByID("5", _color);
			break;
		}
		*/
	}

	private void UpdateColor() {
		if(curColor == Color.white)
		{
			switch(AreaID) 
			{
				default:
				return;
				case 1:
				ChangeLightByID("1", curColor);
				ChangeLightByID("2", curColor);
				ChangeLightByID("4", curColor);
				ChangeLightByID("5", curColor);
				break;
				case 2:
				ChangeLightByID("1", curColor);
				ChangeLightByID("2", curColor);
				ChangeLightByID("4", curColor);
				ChangeLightByID("5", curColor);
				break;
				case 3:
				ChangeLightByID("1", curColor);
				ChangeLightByID("2", curColor);
				ChangeLightByID("4", curColor);
				ChangeLightByID("5", curColor);
				break;
			}
			return;
		}
		Color _curColor = new Color(curColor.r, curColor.g, curColor.b, alpha);
		switch(AreaID) 
		{
			default:
			return;
			case 1:
			ChangeLightByID("1", _curColor);
			ChangeLightByID("2", _curColor);
			ChangeLightByID("4", _curColor);
			ChangeLightByID("5", _curColor);
			break;
			case 2:
			ChangeLightByID("1", _curColor);
			ChangeLightByID("2", _curColor);
			ChangeLightByID("4", _curColor);
			ChangeLightByID("5", _curColor);
			break;
			case 3:
			ChangeLightByID("1", _curColor);
			ChangeLightByID("2", _curColor);
			ChangeLightByID("4", _curColor);
			ChangeLightByID("5", _curColor);
			break;
		}

		Invoke("UpdateColor", 0.1f);
	}
}
