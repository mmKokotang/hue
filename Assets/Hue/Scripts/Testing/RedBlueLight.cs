﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBlueLight : MonoBehaviour {

	public HueLamp[] hueLamps;


	// Use this for initialization
	void Start () {
		ChangeRed();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			ChangeRed();
		}
		if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			ChangeBlue();
		}
	}

	void ChangeRed() {
		for(int i = 0; i < hueLamps.Length; i++)
		{
			hueLamps[i].color = Color.red;
		}
		Invoke("ChangeBlue", 1f);
	}

	
	void ChangeBlue() {
		for(int i = 0; i < hueLamps.Length; i++)
		{
			hueLamps[i].color = Color.blue;
		}
		Invoke("ChangeRed", 1f);
	}
}
