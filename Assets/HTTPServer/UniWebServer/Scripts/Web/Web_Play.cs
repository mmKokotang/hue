﻿using UnityEngine;
using System.Collections;

namespace UniWebServer
{
    [RequireComponent(typeof(EmbeddedWebServerComponent))]
    public class Web_Play : MonoBehaviour, IWebResource
    {
        public string path = "/play";
        public TextAsset html;

        EmbeddedWebServerComponent server;
        HTTPServerComponent httpServerComponent;

        public BoxController boxController;
        public Testing testing;

        void Start ()
        {
            server = GetComponent<EmbeddedWebServerComponent>();
            server.AddResource(path, this);
            httpServerComponent = GetComponent<HTTPServerComponent>();
            #if !UNITY_ANDROID
                Application.OpenURL("http://" + "localhost" + ":" + server.port + path);
            #endif
        }
	
        public void HandleRequest (Request request, Response response)
        {
            response.statusCode = 200;
			response.message = "Someone Login Play";
            Debug.Log(request.body);
            if(request.body == null)
            {
                string tmpString = html.text.Replace("give_thisPageID", "0");
                tmpString = tmpString.Replace("give_ip", "\'" + "localhost" + "\'");
                tmpString = tmpString.Replace("give_port", "\'" + httpServerComponent.ThisPort.ToString() + "\'");
                response.Write(tmpString);
            } else {
                response.Write("123");
                string[] splitStr = request.body.Split('&');
                for(int i = 0; i < splitStr.Length; i++)
                {
                    Debug.Log(splitStr[i].Split('=')[0] + " = " + splitStr[i].Split('=')[1]);
                    if(splitStr[i].Split('=')[0] == "btn")
                    {
                        switch(splitStr[i].Split('=')[1]) {
                            case "Jump":
                            break;
                            case "Up":
                                boxController.OnUpClicked();
                                testing.ChangeBlue();
                            break;
                            case "Down":
                                boxController.OnDownClicked();
                                testing.ChangeRed();
                            break;
                            case "Left":
                                boxController.OnLeftClicked();
                            break;
                            case "Right":
                                boxController.OnRightClicked();
                            break;
                            case "Close":
                                testing.ChangeLightClose();
                            break;
                        }
                    }
                }
                
            }
        }
    }
}