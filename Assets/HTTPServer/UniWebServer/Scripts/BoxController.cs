﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnUpClicked() {
		transform.Translate(Vector3.up * 1);
	}

	public void OnDownClicked() {
		transform.Translate(Vector3.down * 1);

	}

	public void OnLeftClicked() {

		transform.Translate(Vector3.left * 1);
	}

	public void OnRightClicked() {
		transform.Translate(Vector3.right * 1);

	}
}
