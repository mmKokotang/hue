using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class Testing : MonoBehaviour
{
    public string ip;

    private GameUtil gameUtil;

    // Start is called before the first frame update
    void Start()
    {
        gameUtil = GameUtil.GetInstance();
        Display.displays[0].Activate();
        Display.displays[1].Activate();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.A))
        {
            WWWForm form = new WWWForm();
            form.AddField("id", 0);
            form.AddField("btn", "Up");
            //StartCoroutine("Post", form);
            gameUtil.PostController.Post(ip, form);
        }

        if(Input.GetKeyUp(KeyCode.D))
        {
            WWWForm form = new WWWForm();
            form.AddField("id", 0);
            form.AddField("btn", "Down");
            //StartCoroutine("Post", form);
            gameUtil.PostController.Post(ip, form);
        }

        if(Input.GetKeyUp(KeyCode.Escape))
        {
            WWWForm form = new WWWForm();
            form.AddField("id", 0);
            form.AddField("btn", "Close");
            //StartCoroutine("Post", form);
            gameUtil.PostController.Post(ip, form);
        }

        if(Input.GetKeyUp(KeyCode.L))
        {
            ChangeBlue();
        }
        if(Input.GetKeyUp(KeyCode.J))
        {
            ChangeRed();
        }
        if(Input.GetKeyUp(KeyCode.G))
        {
            ChangeLightClose();
        }
    }

    IEnumerator Post(WWWForm _form) {
        using (UnityWebRequest www = UnityWebRequest.Post("http://192.168.11.102:8079/control", _form))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("Form upload complete!");
            }
        }
    }


    public void ChangeBlue() {
        Color color = Color.blue;
        string[] lightsID = new string[]{"1", "2", "4", "5"};
        for(int i = 0; i < lightsID.Length; i++)
        {
            gameUtil.HueBridge.ChangeLightByID(lightsID[i], color);
        }
    }

    
    public void ChangeRed() {
        Color color = Color.red;
        string[] lightsID = new string[]{"1", "2", "4", "5"};
        for(int i = 0; i < lightsID.Length; i++)
        {
            gameUtil.HueBridge.ChangeLightByID(lightsID[i], color);
        }
    }

    public void ChangeLightClose()
    {
        Color color = new Color(0, 0, 0, 0);
        string[] lightsID = new string[]{"1", "2", "4", "5"};
        for(int i = 0; i < lightsID.Length; i++)
        {
            gameUtil.HueBridge.ChangeLightByID(lightsID[i], color);
        }
    }
}
