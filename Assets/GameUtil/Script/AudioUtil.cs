﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class AudioUtil
{

    private static AudioUtil audioUtil;

    public static AudioUtil GetInstance()
    {
        if (audioUtil == null)
            audioUtil = new AudioUtil();
        return audioUtil;
    }

    private AudioUtil()
    {
    }
}
