﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public enum Mode {
    Normal,
    Tour
}

public class GameUtil
{
    public PostController PostController {
        get { 
            if(postController == null)
                postController = GameObject.Find("PostController").GetComponent<PostController>();
            return postController; }
    }
    private PostController postController;

    public HueBridge HueBridge {
        get {
            if(hueBridge == null)
                hueBridge = GameObject.Find("Hue Bridge").GetComponent<HueBridge>();
            return hueBridge;
        }
    }
    private HueBridge hueBridge;

    private static GameUtil gameUtil;

    public static GameUtil GetInstance()
    {
        if (gameUtil == null)
            gameUtil = new GameUtil();
        return gameUtil;
    }

    private GameUtil()
    {
        if(postController == null)
            postController = GameObject.Find("PostController").GetComponent<PostController>();
        if(hueBridge == null)
            hueBridge = GameObject.Find("Hue Bridge").GetComponent<HueBridge>();
    }

    public void Post(string _ip, WWWForm _form) {
        PostController.Post(_ip, _form);
    }

    // Timestamp 轉換成 DateTime
	public static DateTime UnixTimeStampToDateTime(long unixTimeStamp)
    {
        DateTime dtDateTime = new DateTime(1970, 1, 1, 8, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
        return dtDateTime;
    }
    
    // DateTime 轉換成 Timestamp
	public static long UnixDateTimeToTimeStamp(DateTime dateTime)
    {
        long unixTimestamp = (long)(dateTime.Subtract(new System.DateTime(1970, 1, 1, 8, 0, 0))).TotalSeconds;
        return unixTimestamp;
    }
}
