EmbeddedWebServerComponent controls GET POST.
HTTPServerComponent controls files and folders.
They need to have different port.
You can change the connection port at "Server" game object.

Setup:
PC:
Server path is "Assets/StreamingAssets/App"

Android:
Server path is "Resources/App"
When imported image, set it "Read/Write Enabled"
When imported audio, set it Load Type to "Decompress On Load"
In "Player Setting", set "Write Permission" to "External(SDCard)"
*In this demo, only can convert "Texture2D", "AudioClip" and "TextAsset"

